Endless Sequencer
=================

Little sequencer based on the OP-1 endless sequencer

![](assets/ui.png)

## USAGE

- Hold REC and press petals 2-8 to record a sequence in order
- Press petal 1 to add a silence
- Press petal 9 to reset the sequence

## TODO

* multiple channels
* velocity support

## NOTE

* It's not endless, memory gets exhausted around 1M notes
