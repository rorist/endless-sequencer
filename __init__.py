import bl00mbox
import captouch
import leds
import time
from random import randrange

from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m.ui import colours
from ctx import Context

class EndlessSequencer(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.DEBUG = False
        self.is_loading = True
        self.loading_text = 'Loading'
        self.blm = bl00mbox.Channel("Endless Sequencer")
        self.blm.volume = 8000
        self.kick = None
        self.notes = []
        self.samples = []
        self.leds_ids = list(range(40))
        self.sample_names = [
                'kick.wav',
                'snare.wav',
                'rimshot.wav',
                'closed.wav',
                'ride.wav',
                'crash.wav',
                'cowbell.wav',
                ]
        #self.sample_names = [
        #        'kick.wav' ]
        self.app_path = f"{self.app_ctx.bundle_path}/assets/"
        self.current_sample = None
        self.current_note = 0
        self.startTime = time.ticks_ms()
        self.SILENCE = 99
        self.bpm = 120
        self.time_signature = [0.5, 2, 1]
        self.time_signature_txt = ['1/16', '1/8', '1/4']
        self.current_time_signature = 1 # 1/8

    def draw(self, ctx: Context) -> None:
        ctx.rgb(*colours.BLACK)
        ctx.rectangle(
            -120.0,
            -120.0,
            240.0,
            240.0,
        ).fill()
        ctx.save()

        ctx.move_to(0, -20)
        ctx.text_baseline = ctx.MIDDLE
        ctx.text_align = ctx.CENTER
        ctx.rgb(*colours.GO_GREEN)
        ctx.font = "Camp Font 1"
        ctx.font_size = 34
        ctx.text("3NDLESS SEQ")

        if self.is_loading:
            # Loading UI
            ctx.move_to(0, 40)
            ctx.rgb(*colours.RED)
            ctx.font_size = 20
            ctx.text('{} ... {}/{}'.format(self.loading_text,
                                   len(self.samples),
                                   len(self.sample_names)))
        else:
            # Main UI
            ctx.rgb(*colours.WHITE)

            # Buttons
            ctx.move_to(0, -80)
            ctx.font_size = 80
            ctx.text("^")
            ctx.move_to(0, -84)
            ctx.font_size = 18
            ctx.text("rec")

            ctx.rotate(-0.45)
            ctx.move_to(-20, -85)
            ctx.font_size = 80
            ctx.text("^")
            ctx.move_to(-16, -86)
            ctx.font_size = 18
            ctx.text("rst")
            ctx.rotate(0.45)

            ctx.rotate(0.45)
            ctx.move_to(20, -85)
            ctx.font_size = 80
            ctx.text("^")
            ctx.move_to(16, -86)
            ctx.font_size = 18
            ctx.text("nxt")
            ctx.rotate(-0.45)

            # CURRENT SAMPLE
            if self.DEBUG and self.current_sample != None and self.current_sample < len(self.sample_names):
                ctx.move_to(0, 10)
                ctx.rgb(*colours.RED)
                ctx.font_size = 20
                ctx.text(self.sample_names[self.current_sample])

            # BPM
            ctx.text_align = ctx.LEFT
            ctx.move_to(-90, 35)
            ctx.rgb(*colours.WHITE)
            ctx.font_size = 18
            ctx.text("BPM: {}".format(self.bpm))

            # Time signature
            ctx.text_align = ctx.LEFT
            ctx.move_to(-90, 50)
            ctx.rgb(*colours.WHITE)
            ctx.font_size = 18
            ctx.text("TIME: {}".format(self.time_signature_txt[self.current_time_signature]))

            # Length
            ctx.move_to(30, 45)
            ctx.rgb(*colours.BLUE)
            ctx.font_size = 48
            if self.current_note > 0:
                ctx.text("{}".format(self.current_note))
            else:
                ctx.text("{}".format(len(self.notes)))

            # Hint
            ctx.move_to(-80, 70)
            ctx.rgb(*colours.GREY)
            ctx.font_size = 12
            ctx.text("btn to change\nbpm and time")

        ctx.restore()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        # Load samples
        if len(self.sample_names) != len(self.samples) and self.is_loading:
            next = len(self.samples)
            spl = self.sample_names[next]
            if self.DEBUG: 
                print("Loading {}".format(spl))

            sample = self.blm.new(bl00mbox.patches.sampler, self.app_path + spl)
            sample.signals.output = self.blm.mixer
            self.samples.append(sample)
        else:
            #if self.DEBUG:
            #    print("Loading finished.")
            self.is_loading = False

        ct = captouch.read()

        # Input command
        btn = self.input.buttons.app
        if btn.left.repeated or btn.left.pressed:
            self.bpm -= 1
        if btn.right.repeated or btn.right.pressed:
            self.bpm += 1

        if ct.petals[9].pressed: #reset
            if self.DEBUG:
                print("CLEAR SEQ")
            self.notes = []
            self.current_note = 0
            self.current_sample = 0

        if ct.petals[0].pressed: #record
            if self.DEBUG:
                print("REC SEQ")

            self.current_note = 0 # Always restart when recoring

            # Get sequence
            for i in range(2, 9):
                sample_id = i-2
                if ct.petals[i].pressed and sample_id < len(self.sample_names):
                    if self.DEBUG:
                        print('  seq {}'.format(self.sample_names[sample_id]))
                    self.notes.append(sample_id)
                    self.play_sample(sample_id)
                    time.sleep(0.15)
            if ct.petals[1].pressed:
                self.notes.append(self.SILENCE)
                time.sleep(0.15)

        # Light petal
        for i in range(10):

            if ct.petals[i].pressed or \
                    (self.current_sample == i-2
                     and not ct.petals[0].pressed
                     and len(self.notes) > 0):
                leds.set_all_rgb(0, 0, 0)

                # Select led range
                led_id = i*4
                if i == 0: range_from = self.leds_ids[led_id-3:]
                else: range_from = self.leds_ids[led_id-3:led_id]
                leds_range = range_from+self.leds_ids[led_id:led_id+4]

                if self.DEBUG:
                    print("light petal {}".format(i))
                    print("  light leds {}".format(leds_range))

                # Turn on leds
                #colour = (randrange(200), randrange(200), randrange(200))
                for led in leds_range:
                    # Action boutons
                    if i == 0:
                        leds.set_rgb(led, 200, 0, 0)
                    elif i == 1:
                        leds.set_rgb(led, 0, 0, 200)
                    elif i == 9:
                        leds.set_rgb(led, 200, 0, 200)
                    else:
                        leds.set_rgb(led, 0, 200, 0)
                        #leds.set_rgb(led, colour[0], colour[1], colour[2])

        leds.update()

        # Play sequence if nothing is pressed
        if len(self.notes) > 0 and not ct.petals[0].pressed:
            self.play_seq()

    def next_bpm(self):
        # taken from binarybrain code
        beatTime = 60.0 * 1000.0 / self.bpm / self.time_signature[self.current_time_signature]
        curRelTime = time.ticks_diff(time.ticks_ms(), self.startTime)
        return curRelTime / beatTime > 1

    def play_sample(self, i):
        self.current_sample = i
        if i < len(self.samples):
            if self.DEBUG and i in self.sample_names:
                print (self.sample_names[i])
            self.samples[i].signals.trigger.start()

    def play_seq(self):
        if self.DEBUG:
            print("PLAY SEQ")

        if self.next_bpm():
            self.startTime = time.ticks_ms()
            if self.current_note == len(self.notes): self.current_note = 0

            if self.DEBUG:
                print("  play note {}".format(i))

            note_id = self.notes[self.current_note]
            if note_id != 99:
                self.play_sample(note_id)
            self.current_note += 1
        else:
            if self.DEBUG:
                print("  wait")

if __name__ == "__main__":
    import st3m.run
    st3m.run.run_view(EndlessSequencer(ApplicationContext()))

